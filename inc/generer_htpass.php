<?php

/***************************************************************************\
 *  SPIP, Système de publication pour l'internet                           *
 *                                                                         *
 *  Copyright © avec tendresse depuis 2001                                 *
 *  Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribué sous licence GNU/GPL.     *
 *  Pour plus de détails voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

/**
 * Générer un htpass pour chaque utilisateur
 *
 * @package SPIP\Inc\htpass
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Generer un htpass comprehensible par Apache,
 * selon l'algo configure en fonction de la version Apache que l'on utilise
 *
 * https://git.spip.net/spip-team/securite/issues/3724
 *
 * @param string $pass
 * @return string
 */
function inc_generer_htpass_dist(string $pass): string {
	static $config_algo = null;
	if (is_null($config_algo)) {
		include_spip('inc/config');
		$config_algo = lire_config('htpasswd/algo', 'bcrypt');
	}

	switch ($config_algo) {
		// pour Apache < 2.2.17
		case 'crypt':
			$htpass = crypt($pass, crypt_initialiser_sel());
			break;

		// pour Apache >=2.2.18 et < 2.4
		case 'md5':
			try {
				$htpass = crypt_apr1_md5($pass);
			}
			catch (Exception $e) {
				spip_log('Echec calcul crypt_apr1_md5()', 'htpasswd' . _LOG_ERREUR);
				$htpass = null;
			}
			break;

		// pour Apache >= 2.4
		case 'bcrypt':
		default:
			$htpass = password_hash($pass, PASSWORD_BCRYPT);
			break;
	}

	return $htpass ?: '';
}



/**
 * Initialiser le sel htsalt pour crypt si cela n'a pas déjà été fait.
 *
 * @return string
 */
function crypt_initialiser_sel(): string {
	static $htsalt = null;

	if (is_null($htsalt)) {
		include_spip('inc/acces');
		$htsalt = '$1$' . creer_pass_aleatoire();
	}

	return $htsalt;
}


/**
 * @param string $password
 * @param string|null $salt
 * @return string
 *
 * @ref https://stackoverflow.com/a/8786956
 * @throws Exception
 */
function crypt_apr1_md5(string $password, ?string $salt = null): string {
	if (!$salt) {
		$salt = substr(base_convert(bin2hex(random_bytes(6)), 16, 36), 1, 8);
	}
	$len = strlen($password);

	$text = $password . '$apr1$' . $salt;

	$bin = pack('H32', md5($password . $salt . $password));

	for ($i = $len; $i > 0; $i -= 16) {
		$text .= substr($bin, 0, min(16, $i));
	}

	for ($i = $len; $i > 0; $i >>= 1) {
		$text .= ($i & 1) ? chr(0) : $password[0];
	}

	$bin = pack('H32', md5($text));

	for ($i = 0; $i < 1000; $i++) {
		$new = ($i & 1) ? $password : $bin;

		if ($i % 3) {
			$new .= $salt;
		}

		if ($i % 7) {
			$new .= $password;
		}

		$new .= ($i & 1) ? $bin : $password;
		$bin = pack('H32', md5($new));
	}

	$tmp = '';

	for ($i = 0; $i < 5; $i++) {
		$k = $i + 6;
		$j = $i + 12;

		if ($j == 16) {
			$j = 5;
		}

		$tmp = $bin[$i] . $bin[$k] . $bin[$j] . $tmp;
	}

	$tmp = chr(0) . chr(0) . $bin[11] . $tmp;
	$tmp = strtr(
		strrev(substr(base64_encode($tmp), 2)),
		'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',
		'./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
	);

	return '$' . 'apr1' . '$' . $salt . '$' . $tmp;
}
