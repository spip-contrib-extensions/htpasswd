<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/aide.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'htpasswd_description' => 'Ce plugin permet de générer des htpasswd Apache pour les utilisateurs.',
	'htpasswd_nom' => 'htpasswd',
	'htpasswd_slogan' => 'Générer des htpasswd Apache'
);
